from django.shortcuts import render, redirect
from django.http import HttpResponse
from app.models import Genre, Artiste, SubGenre, ArtisteForm, SubGenreForm
from django.conf import settings
from django.core.files.storage import FileSystemStorage
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.http import HttpResponseRedirect

def index(request):
    return render(request, 'index.html')

def home(request):
    genre = Genre.objects.all()
    return render(request, 'main.html',{'list_genre': genre})

def registerView(request):
    if request.method == "POST":
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('login_url')
    else:
        form = UserCreationForm()

    return render(request, 'registration/register.html',{'form':form})


def listeArtiste(request, id):
    genre = Genre.objects.get(id=id)
    artiste = Artiste.objects.filter(genre = genre,user=request.user).order_by('subGenre')
    subGenre = SubGenre.objects.all()
    return render(request, 'listeArtiste.html', {'genre': genre, 'artiste': artiste, subGenre: 'subGenre', 'name' : request.user.username })

def ficheArtiste(request, id):
    artiste = Artiste.objects.get(id = id)
    return render(request,'ficheArtiste.html',{'artiste':artiste})
            
def addArtiste(request):
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        form = ArtisteForm(request.user, request.POST)
        f = form.save(commit=False)
        f.user = request.user
        f.save() 
        return HttpResponseRedirect('accueil')
    # if a GET (or any other method) we'll create a blank form
    else:
        form = ArtisteForm(request.user)
    return render(request,'addArtiste.html', {'form': form})

def addSubGenre(request, id):
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        form = SubGenreForm(request.POST)
        f = form.save(commit=False)
        f.user = request.user
        f.save()
        if id == 0:
            return HttpResponseRedirect('/accueil')
        if id == 1:
            return HttpResponseRedirect('/addArtiste')
    # if a GET (or any other method) we'll create a blank form
    else:
        form = SubGenreForm()
    return render(request,'addSubGenre.html', {'form': form})