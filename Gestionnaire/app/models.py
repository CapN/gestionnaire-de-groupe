from django.db import models
from django.forms import ModelForm
from django.utils import timezone
from django.contrib.auth.models import User
from django import forms

class Artiste(models.Model):
    nom = models.CharField(max_length=100)
    genre = models.ForeignKey('Genre', on_delete= models.CASCADE)
    subGenre = models.ForeignKey('SubGenre', null = True,blank = True,  on_delete= models.CASCADE)
    desc = models.TextField(null=True, blank=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE, null = True,blank = True)


    def __str__(self):
        return self.nom

class Album(models.Model):
    nom = models.CharField(max_length=100)
    artiste = models.ForeignKey('Artiste', on_delete=models.CASCADE)    

    def __str__(self):
        return self.nom

class Genre(models.Model):
    nomGenre = models.CharField(max_length=100)

    def __str__(self):
        return self.nomGenre

class SubGenre(models.Model):
    nomSubGenre = models.CharField(max_length=100)
    nomGenre = models.ForeignKey('Genre', on_delete= models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE, null = True,blank = True)
    def __str__(self):
        return self.nomSubGenre

class ArtisteForm(ModelForm):
    class Meta:
        model = Artiste
        fields = ['nom', 'genre','subGenre','desc']

    def __init__(self, user, *args, **kwargs):
        super(ArtisteForm, self).__init__(*args, **kwargs)
        self.fields['subGenre'].queryset = SubGenre.objects.filter(user=user)

class SubGenreForm(ModelForm):
    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user',None)
        super(SubGenreForm, self).__init__(*args, **kwargs)

    
    class Meta:
        model = SubGenre
        fields = [ 'nomSubGenre', 'nomGenre',]
