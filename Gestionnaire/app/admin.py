from django.contrib import admin
from .models import Artiste, Album, Genre, SubGenre

admin.site.register(Artiste)
admin.site.register(Album)
admin.site.register(Genre)
admin.site.register(SubGenre)