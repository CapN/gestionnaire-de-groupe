"""Gestionnaire URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from app import views
from django.conf import settings
from django.conf.urls.static import static
from django.contrib.auth.views import LoginView, LogoutView
urlpatterns = [
    path('admin/', admin.site.urls),
    path('index/',views.index, name = 'index'),
    path('register/', views.registerView, name = "register_url"),
    path('login/',LoginView.as_view(), name ="login_url"),
    path('logout/',LogoutView.as_view(next_page ='index'), name ="logout_url"),
    path('accueil', views.home, name = "accueil"),
    path('Genre/<int:id>', views.listeArtiste, name='listeArtiste'),
    path('Artiste/<int:id>',views.ficheArtiste, name='ficheArtiste'),
    path('addArtiste',views.addArtiste, name='addArtiste'),
    path('addSubGenre/<int:id>',views.addSubGenre, name='addSubGenre'),
]
if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)